# Rotating Backup Script

This is a simple 7-day plus EOM rotating backup script for MS SQL DBs written as a Windows batch file.  It does the following:
* Creates backups for all specified DB files
* Saves those backps uncompressed in folders named by the day of the week
* Compresses that folder with 7-Zip
* Stores the compressed file on the main drive and a second external or mapped drive
* Deletes the previous backups for that day of the week before starting a new backup
* Stores a copy to be kept for a month if it is the last day of the month
* Keeps a log file for what it is doing, capped at 500 lines before it will create a new file, it will save one old copy


I run it daily via Task Manager on our customers' DB servers.  You'll need to have 7-Zip's 7sa.dll and 7za.exe in the same folder as the batch file.  It can be downloaded from https://www.7-zip.org/download.html.  You want the "7-Zip Extra: standalone console version, 7z DLL".

I wrote this script for a very specific use case when I couldn't find anything else to do what I needed on our little deployed SQL servers that are largely isolated from the world.  Take it, rip it apart, make it do what you want it to do.  Right now, it only backs up SQL files.  But, you could easily use the same logic to keep rotating backups of certain files or folders.  If I really had the desire, I would add in some logic to set event codes whenever a backup finishes or errors out.  But, there isn't any monitoring in place to create alerts from those events so I haven't put in the work.  Maybe one day this company will get there and it would be useful, but that isn't today.

Have fun, I'm sure most of you could write something much better.  But, it solved my problem when I needed it to and maybe it will help someone else.