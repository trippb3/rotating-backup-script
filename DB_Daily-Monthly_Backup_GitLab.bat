﻿
@echo off

REM   Written by Tripp Beasley

REM   Edit the section below to match the local installation that is labeled EDIT THIS SECTION

REM   Create Variables
	setlocal EnableDelayedExpansion
	For /F "tokens=1,2,3,4 delims=/ " %%A in ('Date /t') do @( 
	Set DOW=%%A
	Set Month=%%B
	Set Day=%%C
	Set Year=%%D
	)

REM   ↓↓↓↓↓ EDIT THIS SECTION ↓↓↓↓↓ 
	set BackupDir=C:\backup
	set ExtBackupDir=e:\backup
	set dbUsername1=sa
	set dbPassword1=saPassword
	set dbServer1=.\instance1Name
	set dbList1=DB1 DB2 DB3
	REM   set dbUsername2=sa
	REM   set dbPassword2=saPassword
	REM   set dbServer2=.\instance2Name
	REM   set dbList2=DB4 DB5 DB6
REM   ↑↑↑↑↑ END EDIT SECTION ↑↑↑↑↑

	set BackupComp=%BackupDir%\Compressed
	Set TempDir=%BackupsDir%\Temp
	Set LogFile=%BackupDir%\backup.log
	Set OldLog=%BackupDir%\backup.old.log

REM   Move Out Old Log File
	set "cmd=findstr /R /N "^^" %LogFile% | find /C ":""
	for /f %%a in ('!cmd!') do set number=%%a
	IF %number% GEQ 500 (
		del %OldLog%
		move %LogFile% %OldLog%
	) 

REM   Start Log File
	ECHO ****************************************************** >> %LogFile%
	ECHO Beginning backup for %DOW% on %DATE% %TIME% >> %LogFile%
	
REM   Remove Old/Create New Daily BackupDir and Compressed Backup
	echo %time% %date% Removing old daily backups >> %LogFile%
	rmdir %BackupDir%\%DOW%\ /s /q
	mkdir %BackupDir%\%DOW%
	del %BackupComp%\*%DOW%* /q
	del %ExtBackupDir%\*%DOW%* /q
	
REM   Create SQL Backups
	ECHO %time% %date% Starting %DOW% daily backups >> %LogFile%
	for %%a in (%dbList1%) do (
			echo %time% %date% Starting %%a backup >> %LogFile%
			OSQL -U %dbUsername1% -P %dbPassword1% -S %dbServer1% -Q "BACKUP DATABASE %%a TO DISK = '%BackupDir%\%DOW%\%%a-%DOW%.bak' WITH FORMAT" >> %LogFile%
			echo %time% %date% Finished %%a backup >> %LogFile%
	)
	for %%a in (%dbList2%) do (
			echo %time% %date% Starting %%a backup >> %LogFile%
			OSQL -U %dbUsername2% -P %dbPassword2% -S %dbServer2% -Q "BACKUP DATABASE %%a TO DISK = '%BackupDir%\%DOW%\%%a-%DOW%.bak' WITH FORMAT" >> %LogFile%
			echo %time% %date% Finished %%a backup >> %LogFile%
			)
			ECHO %time% %date% Finished with %DOW% daily backups >> %LogFile%

REM   Compress Daily Backups/Move To External
	ECHO %time% %date% Compressing daily backups and moving to external drive >> %LogFile%
	%BackupDir%\7za.exe a %BackupComp%\%DOW%-Backup.zip %BackupDir%\%DOW%\* -mx7 >> %LogFile%
	copy %BackupComp%\%DOW%-Backup.zip %ExtBackupDir%
	ECHO %time% %date% Done compressing backups and moving to external drive >> %LogFile%
	
REM   Check for EOM
	set Feb=28
	set /a Leap=year %% 4
	if "%Leap%"=="0" set Feb=29
	set i=101
	for %%a in (31 %Feb% 31 30 31 30 31 31 30 31 30 31) do (
		if !i!==1%Month% set LastDay=%%a
		set /a i+=1
	)
	if %Day%==%LastDay% (
		echo %time% %date% Today is the last day of the month.  Preparing to run EOM backup.
		goto RUNEOM
		) else (
		echo %time% %date% Today is not the last day of the month.  Skipping EOM backup.
		goto CLOSELOG
	)

:RUNEOM
REM   Delete Old EOM Backup and Compressed Backup
	echo %time% %date% Deleting old EOM files >> %LogFile%
	rmdir %BackupDir%\Month\ /s /q
	mkdir %BackupDir%\Month
	del %BackupComp%\*Month* /q
	del %ExtBackupDir%\*Month* /q

REM   Create EOM Backup
	ECHO %time% %date% Creating Monthly Backup Copies >> %LogFile%
	for %%a in (%dblist1%) do copy %BackupDir%\%DOW%\%%a-%DOW%.bak %BackupDir%\Month\%%a-Month%Month%.bak
	for %%a in (%dblist2%) do copy %BackupDir%\%DOW%\%%a-%DOW%.bak %BackupDir%\Month\%%a-Month%Month%.bak
	copy %BackupComp%\%DOW%-Backup.zip %BackupComp%\Month%Month%-Backup.zip
	copy %BackupComp%\Month%Month%-Backup.zip %ExtBackupDir%
	ECHO %time% %date% Finshed with Monthly backups >> %LogFile%

:CLOSELOG
	ECHO %time% %date% Ending backup for %DOW% on %DATE% %TIME% >> %LogFile%
	ECHO ****************************************************** >> %LogFile%

